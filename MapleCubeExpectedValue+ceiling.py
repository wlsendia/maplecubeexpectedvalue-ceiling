import random

this_prob = 0.1
master_prob = 0.011858
black_prob = 0.035
master_rank_up = 0
black_rank_up = 0
black_ceiling = 0

for i in range(1000000):
    this_prob = random.random()
    if this_prob < master_prob:
        master_rank_up += 1
    if (this_prob < black_prob) or (42 == black_ceiling):
        black_rank_up += 1
        black_ceiling = 0
    else:
        black_ceiling += 1

print("장큐 갯수" + str(master_rank_up))
print("블큐 갯수" + str(black_rank_up))